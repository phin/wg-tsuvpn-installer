# install openjdk 20
mkdir /usr/lib/jvm/
wget https://download.java.net/java/GA/jdk20.0.2/6e380f22cbe7469fa75fb448bd903d8e/9/GPL/openjdk-20.0.2_linux-x64_bin.tar.gz
tar -xvf openjdk-20.0.2_linux-x64_bin.tar.gz -C /usr/lib/jvm
# angristan's script
curl -O https://raw.githubusercontent.com/angristan/wireguard-install/master/wireguard-install.sh
chmod +x wireguard-install.sh
./wireguard-install.sh
# adding dns servers in case angristan's script fucked up the resolvers
echo 'nameserver 84.200.69.80' >> /etc/resolv.conf
echo 'nameserver 84.200.70.40' >> /etc/resolv.conf
# wirerest installation
mkdir /var/wirerest
wget -O /var/wirerest/wirerest-0.6.jar https://github.com/FokiDoki/WireRest/releases/download/0.6-BETTA/wirerest-0.6.jar
wget -O /etc/systemd/system/wirerest.service https://codeberg.org/phin/wg-tsuvpn-installer/raw/branch/main/wirerest.service
clear
echo "Wireguard + Wirerest installation almost complete!"
echo "Please edit the systemd file to have your desired admin access key in /etc/systemd/system/wirerest.service"
echo "After, do sudo systemctl enable --now wirerest.service"