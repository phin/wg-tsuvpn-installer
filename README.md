# TsuVPN's Wireguard Installer
an installer based off angristan's wg installer that adds the wirerest api (mostly for ez setup on new tsuvpn servers)

You can run a one-line version of the code here:
`wget https://codeberg.org/phin/wg-tsuvpn-installer/raw/branch/main/tsuwg.sh && chmod +x ./tsuwg.sh && ./tsuwg.sh`
Or on a debian minimal server, you can install everything required just to make sure and then run the script:
`apt-get -y update && apt-get -y install sudo wget curl tar && wget https://codeberg.org/phin/wg-tsuvpn-installer/raw/branch/main/tsuwg.sh && chmod +x ./tsuwg.sh && ./tsuwg.sh`

After you install, you have to change Wirerest's API password before doing so. You can either manually edit the systemd file and then enable it, or run `sed -i 's/changeme/your-access-key/g' /etc/systemd/system/wirerest.service && sudo systemctl enable --now wirerest.service`

One thing I'll say is dont use any of the mainstream DNSes. I run my own servers, you should too. Angristan's script which is included uses 1.1.1.1 by default, but replace it with dns.watch or your own DNS server for max privacy.